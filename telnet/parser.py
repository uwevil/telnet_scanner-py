import os

FIELD_SEP = ":"

def perser_database(filename):
    """ Read input file
    """
    lines = []
    with open(filename, 'r') as file:
        lines = file.readlines()

    result = []
    for line in lines:
        user_password = line.split(FIELD_SEP)
        dict_all = {
            "user": user_password[0],
            "password": user_password[1],
        }
        result.append(dict_all)

    return result
