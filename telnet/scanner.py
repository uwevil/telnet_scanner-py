import sys
import telnetlib

DEFAULT_TELNET_PORT = "23"

HOST = "uwevil.ddns.net"
DEFAULT_USER = "admin"
DEFAULT_PASSWORD = "doancaosang"


def connect_ip(host, port=None, user=DEFAULT_USER, password=DEFAULT_PASSWORD):
    """ connect to ip
    """
    if port is None:
        port = DEFAULT_TELNET_PORT
    print("Connecting ... " + host + " " + port)
    conn = telnetlib.Telnet(host, port)
    conn.read_until(b"login: ")
    conn.write((user+ "\n").encode("utf-8"))

    if password:
        conn.read_until(b"Password: ")
        conn.write((password + "\n").encode("utf-8"))

    conn.write(b"ls\n")
    conn.write(b"exit\n")
    print(conn.read_all().decode("utf-8"))
    return host+":"+port+" "+user+":"+password

def connect_range_ip(ip1, ip2, filename, port=None):
    """ Connect to a range ip and using database
    """
    print(ip1)
    print(ip2)
    print(filename)
    print(port)
