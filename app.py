# -*- coding: utf-8 -*-
""" Utilitaire pour scanner les ips qui disponsent une commande
    telnet.

::

    Usage:
        scan (-i <ip>) [--port=<port>]
        scan (<ip1> <ip2> -f <filename>) [--port=<port>]
        scan -h | --help
        scan --version

::

    Options:
        -i <str> [(-p <port>)]                      ip with/without port
        <ip1> <ip2> -f <filename> [--port=<port>]   ip range with/without port
        -h --help                                   show help
        --version                                   show version

"""
import os
import sys

from schema import Or, Schema, SchemaError, Use
from docopt import docopt

from telnet.scanner import connect_ip, connect_range_ip

ARGS_SEPARATOR = " "
VERSION = ""

# Lancement #################################################################

def launch(arguments):
    """ Choix de la création à effectuer en fonction des arguments.

        :param dict args: les arugments donnés par l'utilisateur
            (cf retour de :func:`check_arguments`)
    """
    if arguments.get("ip"):
        host = arguments.get("ip")
        port = None
        if arguments.get("port") is not None:
            port = arguments.get("port")
        connect_ip(host, port=port)
    elif arguments.get("ip1") is not None and arguments.get("ip2") is not None:
        ip1 = arguments.get("ip1")
        ip2 = arguments.get("ip2")
        port = arguments.get("port")
        filename = arguments.get("filename")
        connect_range_ip(ip1, ip2, filename, port=port)


def check_arguments(arguments):
    """ Valide les paramètres de la ligne de commande au sens docopt et
        les sélectionne/reformate pour faire le lien avec la fonction launch.

        Returns:
            dict:
                {"yml": fichier de config existant ou None
                "str": une chaine à chiffrer ou None}

        Arguments:
            arguments (dict): les arguments passés sur la ligne de commande
                 {"-y": chemin du fichier de configuration
                 "-c": chaine à chiffrer
                 "-m": vrai si précisé avec '-m <methode>'
                 "--help": vrai si argument -h ou --help
                 "--version": vrai si argument -v ou --version}
    """
    schema_i = Or(None, str, error="-i error {}".format(arguments.get("-i")))
    schema_f = Or(None, str, error="-f error {}".format(arguments.get("<filename>")))
    schema_p = Or(None, str, error="-p error {}".format(arguments.get("-p")))
    schema_ip1 = Or(None, str, error="ip1 error {}".format(arguments.get("<ip1>")))
    schema_ip2 = Or(None, str, error="ip2 error {}".format(arguments.get("<ip2>")))
    try:
        arguments = Schema({"-i": schema_i,
                            "<filename>": schema_f,
                            "-f": Use(bool),
                            "--port": schema_p,
                            "<ip1>": schema_ip1,
                            "<ip2>": schema_ip2,
                            "--help": Use(bool),
                            "--version": Use(bool)}).validate(arguments)
    except SchemaError as error:
        print(error)
        print(arguments)
        exit()

    return {"ip": arguments.get("-i"),
            "ip1": arguments.get("<ip1>"),
            "ip2": arguments.get("<ip2>"),
            "port": arguments.get("--port"),
            "filename": arguments.get("<filename>")}


if __name__ == '__main__':
    if __name__ == '__main__':
        COMMAND = ARGS_SEPARATOR.join(sys.argv)

    print("Lancement de {}".format(COMMAND))
    ARGS = check_arguments(docopt(__doc__, version=VERSION))
    print(ARGS)
    launch(ARGS)
